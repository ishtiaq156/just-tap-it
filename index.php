<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>
    <title>Just Push It</title>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <link rel="stylesheet" href="/styles/main.css">
</head>

<body>

<div id="scoreValue" hidden>
    <?php
    $server = '192.185.4.60';
    $username = 'ishtiaq1_root';
    $password = 'password';
    $database = 'ishtiaq1_Score';

    // Create connection
    $conn = new PDO("mysql:host=$server;dbname=$database;", $username, $password);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
        echo "fail";
    }

    //$updatesql = mysqli_query($conn,'UPDATE ishtiaq1_Score.Score set ishtiaq1_Score.Score.scoreValue=1666');

    $sql = 'SELECT ishtiaq1_Score.Score.scoreValue FROM ishtiaq1_Score.Score';
    foreach ($conn->query($sql) as $row) {
        print $row['scoreValue'] . "\t";
    }
    ?>
</div>

<div class="header">
    <h1>How many times will it be pushed?</h1>
</div>

<iframe name="formDestination" hidden></iframe>

<form action="/scripts/updateScore.php" target="formDestination" method="post">
    <button id="clickMe" type="submit" hidden>hjhjghj</button>
</form>
<div class="right">
    <a onclick="scoreIncrement()" class="button">Just push it!</a>
</div>

<div class="below">
    <p id="score">Pushed: 0 times</p>
</div>


<div class="footer">
    <p>Made by Ishtiaq Syed</p>
</div>


<script src="https://code.jquery.com/jquery-3.2.1.js"
        integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
        crossorigin="anonymous"></script>
<script src="/scripts/index.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
        integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
        crossorigin="anonymous"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-106212051-1', 'auto');
    ga('send', 'pageview');
    ga('set', 'userId', 'ishtiaq156');

</script>
</body>
</html>